## Phân công công việc

| Công việc                                        | Người thực hiện    |
| ------------------------------------------------ | ------------------ |
| Cơ sở dữ liệu, biểu đồ lớp chi tiết              | Trần Trung Huỳnh   |
| Biểu đồ lớp chi tiết, giao diện SI               | Trần Thị Thu Hương |
| Giao diện người dùng (GUI), biểu đồ lớp chi tiết | Trần Quang Huy     |
| Biểu đồ lớp chi tiết, biểu đồ thực thể liên kết  | Loy Kakda          |

## Review

| Người review       | Người được review  |
| ------------------ | ------------------ |
| Trần Trung Huỳnh   | Trần Quang Huy     |
| Trần Quang Huy     | Loy Kakda          |
| Loy Kakda          | Trần Thị Thu Hương |
| Trần Thị Thu Hương | Trần Trung Huỳnh   |
