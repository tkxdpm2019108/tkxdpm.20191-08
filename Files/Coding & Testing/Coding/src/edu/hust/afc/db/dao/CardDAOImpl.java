/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */

package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;
import edu.hust.afc.entity.station.Station;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Collectors;

import com.sun.istack.NotNull;

public class CardDAOImpl implements CertificateDAO<PrepaidCard> {

    /**
     * Instance of DBConnection help connect with database
     */
    private DBConnection dbConnection;

	public CardDAOImpl(DBConnection dbConnection) {
		this.dbConnection = dbConnection;
	}

    @Override
    public List<PrepaidCard> getDataByIds(@NotNull List<String> ids) {
        return ids.stream().map(this::getDataById).collect(Collectors.toList());
    }

    @Override
    public PrepaidCard getDataById(String id) {
        String query = "SELECT * FROM card where id LIKE '" + id + "'";
        PrepaidCard prepaidCard = null;

        try {
            Connection connection = dbConnection.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                prepaidCard = new PrepaidCard(resultSet);
            }

            int stationId = resultSet.getInt("enter_station_id");
            if (stationId != 0) {
                query = "SELECT  * FROM station WHERE id = " + stationId;
                resultSet = connection.prepareStatement(query).executeQuery();
                if (resultSet.next() && prepaidCard != null) {
                    prepaidCard.setEnterStation(new Station(resultSet));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return prepaidCard;
    }

    @Override
    public boolean updateData(@NotNull PrepaidCard prepaidCard) {
        int enterStationId = prepaidCard.getEnterStation() == null ? 0 : prepaidCard.getEnterStation().getId();
        String query = "UPDATE card " +
                "SET balance = " + prepaidCard.getBalance() + ", status = '" + prepaidCard.getStatus() + "', " +
                "enter_station_id = " + enterStationId + ", bar_code = '" + prepaidCard.getBarCode() + "' " +
                "WHERE id LIKE '" + prepaidCard.getId() + "'";

        try {
            dbConnection.getConnection().prepareStatement(query).executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
