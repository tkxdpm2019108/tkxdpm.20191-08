/**
 * Observer Pattern
 *
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.observer;

public interface Observer<T> {

    /**
     * update data in subject
     * @param data data will be updated
     */
    void update(T data);
}
