/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.dealing;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.sun.istack.NotNull;

public abstract class Dealing {
	/**
	 * Action enter platform area
	 */
	public static final String ACTION_ENTER = "enter";
	/**
	 * Action exit platform area
	 */
	public static final String ACTION_EXIT = "exit";
	/**
	 * Query for insert dealing
	 */
	protected static final String SQL_INSERT_FORMAT = "INSERT INTO mydb.%s VALUES (%d, '%s', '%s', '%s', %d);";
	/**
	 * id of ticket or card
	 */
	protected int id;
	/**
	 * Time for dealing
	 */
	protected String time;
	/**
	 * Action of dealing
	 */
	protected String action;
	/**
	 * Id of station
	 */
	protected int stationId;

	protected Dealing(int id, String time, String action, int stationId) {
		this.id = id;
		this.time = time;
		this.action = action;
		this.stationId = stationId;
	}

	protected Dealing(@NotNull ResultSet resultSet) throws SQLException {
		this.id = resultSet.getInt("id");
		this.time = resultSet.getString("time");
		this.action = resultSet.getString("type");
		this.stationId = resultSet.getInt("station_id");
	}

	/**
	 * Get insert query for update info card/ticket
	 * 
	 * @param tableName
	 * @param index
	 * @return String
	 */
	protected abstract String getInsertQuery(String tableName, int index);
}
