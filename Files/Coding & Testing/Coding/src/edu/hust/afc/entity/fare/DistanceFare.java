/**
 * @author Tran Quang Huy
 * @version 1
 * @since 07/12/2019
 */
package edu.hust.afc.entity.fare;

import edu.hust.afc.entity.station.Station;

public class DistanceFare extends Fare {
	
	/**
	 * base distance 
	 */
    private static final float BASE_DISTANCE = 5f;
	/**
	 * base fare unit
	 */
    private static final float BASE_FARE_UNIT = 1.9f;
	/**
	 * fare over base unit 
	 */
    private static final float FARE_OVER_BASE_UNIT = 0.4f;

    public DistanceFare() {
    }

    public DistanceFare(Station enterStation, Station exitStation) {
        super(enterStation, exitStation);
    }

    @Override
    public float getFare() {
        if (enterStation == null || exitStation == null) return 0f;
        float distance = Math.abs(enterStation.getDistanceToOrigin() - exitStation.getDistanceToOrigin());
        if (distance <= BASE_DISTANCE) return BASE_FARE_UNIT;
        float temp = (distance - BASE_DISTANCE) / 2;
        int multiple = temp == (int) temp ? (int) temp : ((int) temp + 1);
        return BASE_FARE_UNIT + multiple * FARE_OVER_BASE_UNIT;
    }
}
