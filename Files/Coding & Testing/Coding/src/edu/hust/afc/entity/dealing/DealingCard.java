/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.dealing;

public class DealingCard extends Dealing {
	/**
	 * id of card
	 */
    private String cardId;

    public DealingCard(int id, String time, String action, int stationId, String cardId) {
        super(id, time, action, stationId);
        this.cardId = cardId;
    }
    
    /**
	 * Get insert query for update info card/ticket
	 * 
	 * @param tableName
	 * @param index
	 * @return String
	 */

    public String getInsertQuery(String tableName, int index) {
        return String.format(SQL_INSERT_FORMAT, tableName, index, time, action, cardId, stationId);
    }
}
