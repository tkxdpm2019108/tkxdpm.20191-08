/**
 * {@link edu.hust.afc.controller.PrepaidCardController} control logic, execute data about prepaid card
 * Handle event check in or check out with prepaid card
 *
 * @author Tran Quang Huy
 * @version 1
 * @since 27-10-2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingCardDAO;
import edu.hust.afc.entity.dealing.Dealing;
import edu.hust.afc.entity.dealing.DealingCard;
import edu.hust.afc.entity.fare.DistanceFare;
import edu.hust.afc.entity.fare.Fare;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Messages;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class PrepaidCardController implements CertificateController<PrepaidCard> {

    /**
     * List of card in file
     */
    private List<PrepaidCard> prepaidCards = Collections.emptyList();
    /**
     * receive data to update, and notify to screen to show
     */
    private Subject<String> dataDisplay;
    /**
     * Data access object of card
     */
    private CertificateDAO<PrepaidCard> cardDAO;
    /**
     * Data access object of dealing card
     */
    private DealingCardDAO dealingCardDAO;
    /**
     * fare of dealing card
     */
    private Fare fare;
    /**
     * Constructor of PrepaidCardController
     *
     * @param cardDAO
     * @param dealingCardDAO
     * @param dataDisplay
     */
    public PrepaidCardController(CertificateDAO<PrepaidCard> cardDAO, DealingCardDAO dealingCardDAO, Subject<String> dataDisplay) {
        this.cardDAO = cardDAO;
        this.dealingCardDAO = dealingCardDAO;
        this.dataDisplay = dataDisplay;
        fare = new DistanceFare();
    }

    @Override
    public List<PrepaidCard> getCertificatesByIds(List<String> certificateIds) {
        List<String> cardIds = certificateIds.stream().filter(PrepaidCard::isCardId).collect(Collectors.toList());
        prepaidCards = cardDAO.getDataByIds(cardIds);
        return prepaidCards;
    }

    @Override
    public boolean handleCheckIn(String barCodeInput, Station station) {
        PrepaidCard prepaidCard = findCertificateByBarCode(prepaidCards, barCodeInput);
        if (prepaidCard == null) return false;

        if (prepaidCard.isInStation()) {
            dataDisplay.notifyChange(prepaidCard.toInfo());
            dataDisplay.notifyChange(Messages.Card.ERROR_NEED_CHECK_OUT);
            return false;
        }
        prepaidCard.setEnterStation(station);
        prepaidCard.setStatus(PrepaidCard.CARD_IN_STATION);
        cardDAO.updateData(prepaidCard);
        dealingCardDAO.save(createDealing(prepaidCard, station, Dealing.ACTION_ENTER));
        dataDisplay.notifyChange(prepaidCard.toInfo());
        return true;
    }

    @Override
    public boolean handleCheckOut(String barCodeInput, Station station) {
        PrepaidCard prepaidCard = findCertificateByBarCode(prepaidCards, barCodeInput);

        if (prepaidCard == null) return false;

        if (!prepaidCard.isInStation()) {
            dataDisplay.notifyChange(prepaidCard.toInfo());
            dataDisplay.notifyChange(Messages.Card.ERROR_NEED_CHECK_IN);
            return false;
        }

        float money = calculateMoney(prepaidCard.getEnterStation(), station);
        if (money > prepaidCard.getBalance()) {
            dataDisplay.notifyChange(prepaidCard.toInfo());
            dataDisplay.notifyChange(String.format(Messages.ERROR_NOT_ENOUGH_BALANCE, money));
            return false;
        }

        prepaidCard.setEnterStation(null);
        prepaidCard.setStatus(PrepaidCard.CARD_OUT_STATION);
        prepaidCard.setBalance(prepaidCard.getBalance() - money);
        cardDAO.updateData(prepaidCard);

        dealingCardDAO.save(createDealing(prepaidCard, station, Dealing.ACTION_EXIT));

        dataDisplay.notifyChange(prepaidCard.toInfo());
        return true;
    }
    /**
     * create Dealing of DealingCard
     *
     * @param card
     * @param station
     * @param action
     * @return DealingCard
     */
    private DealingCard createDealing(PrepaidCard card, Station station, String action) {
        String time = AFCUtils.getTime(System.currentTimeMillis());
        return new DealingCard(0, time, action, station.getId(), card.getId());
    }
    /**
     * calculate money fare 
     *
     * @param enterStation
     * @param exitStation
     * @return get fare on entered station and exit station
     */
    private float calculateMoney(Station enterStation, Station exitStation) {
        return fare.setEnterStation(enterStation)
                .setExitStation(exitStation)
                .getFare();
    }
}
