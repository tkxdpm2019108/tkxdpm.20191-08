/**
 * @author Tran Quang Huy
 * @version 1
 * @since 07/12/2019
 */
package edu.hust.afc.utils;

public interface Messages {
    String ERROR_INVALID_INPUT = "[ERROR] Invalid Input !";
    String ERROR_NOT_ENOUGH_BALANCE = "[ERROR] Not enough balance: Expected %f euros.";

    interface Card {
        String ERROR_NEED_CHECK_IN = "[ERROR] You must to check in with this card first !!";
        String ERROR_NEED_CHECK_OUT = "[ERROR] You must to check out with this card first !!";
    }

    interface Ticket {
        String SUCCESS_VALID_TICKET = "[SUCCESS] Your ticket is valid !";
        String ERROR_DESTROYED_TICKET = "[ERROR] Your ticket was destroyed !";
        String ERROR_IN_USE_TICKET = "[ERROR] Your ticket is in use !";
        String ERROR_NEW_TICKET = "[ERROR] Your ticket is new !";
        String ERROR_NEED_CHECK_OUT = "[ERROR] Ticket is in station or destroy, You must check out first!";
        String ERROR_NEED_CHECK_IN = "[ERROR] Ticket is new, You must check in first!";
        String ERROR_INVALID_24h_TICKET = "Invalid 24h Ticket\n";
        String ERROR_EXPIRED_24h_TICKET = "Expired: Try to enter at ";
        String ERROR_CAN_NOT_ENTER_STATION = "[ERROR] You can not enter this station because this station don't belong to platform area !";
    }
}
