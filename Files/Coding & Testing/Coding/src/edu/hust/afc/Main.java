package edu.hust.afc;

import edu.hust.afc.boundary.Screen;
import edu.hust.afc.utils.Messages;
import hust.soict.se.customexception.InvalidIDException;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String inputContinue;

        try {
            do {
                inputContinue = "";
                Screen screen = new Screen();
                screen.displayStations();
                screen.displayAllTicketsAndCards();

                System.out.println("\n================================================================\n");
                System.out.print("Do you want to be continue (yes/no)? \t\t");
                inputContinue = scanner.nextLine();
                System.out.println("\n================================================================");
            } while ("yes".equals(inputContinue.trim()));
        } catch (InvalidIDException e) {
            System.out.println(Messages.ERROR_INVALID_INPUT);
            System.exit(0);
        }
    }
}
