package testing.java.afc.entity.fare;

import edu.hust.afc.entity.fare.DistanceFare;
import edu.hust.afc.entity.station.Station;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DistanceFareTest {

    @InjectMocks
    DistanceFare distanceFare;

    @Mock
    Station enterStation;

    @Mock
    Station exitStation;

    @Before
    public void setUp() {
        distanceFare = new DistanceFare(enterStation, exitStation);
    }

    @Test
    public void getFare_longerThan5kms() {

        when(enterStation.getDistanceToOrigin()).thenReturn(15.8f);
        when(exitStation.getDistanceToOrigin()).thenReturn(8.5f);

        final float expected = 2.7f;
        final float actual = distanceFare.getFare();
        final float delta = 0.1f;
        Assert.assertEquals(expected, actual, delta);
    }

    @Test
    public void getFare_shorterThan5kms() {
        when(enterStation.getDistanceToOrigin()).thenReturn(11.3f);
        when(exitStation.getDistanceToOrigin()).thenReturn(8.5f);
        final float expected = 1.9f;
        final float acutal = distanceFare.getFare();
        final float delta = 0.1f;
        Assert.assertEquals(expected, acutal, delta);
    }
}
