## Phân công công việc

| Công việc                             | Người thực hiện    |
| ------------------------------------- | ------------------ |
| Test các thứ liên quan đến vé 1 chiều | Trần Trung Huỳnh   |
| Test các thứ liên quan đến vé 24h     | Trần Thị Thu Hương |
| Test các thứ liên quan đến thẻ        | Trần Quang Huy     |
|                                       | Loy Kakda          |

## Review

| Người review       | Người được review  |
| ------------------ | ------------------ |
| Trần Trung Huỳnh   | Trần Quang Huy     |
| Trần Quang Huy     | Loy Kakda          |
| Loy Kakda          | Trần Thị Thu Hương |
| Trần Thị Thu Hương | Trần Trung Huỳnh   |
