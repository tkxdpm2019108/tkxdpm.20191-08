﻿## Phân công công việc

| Công việc                                                                                                     | Người thực hiện    |
| ------------------------------------------------------------------------------------------------------------- | ------------------ |
| Vẽ biểu đồ trình tự và class tổng quan cho usecase soát vé vào/ra 1 chiều, class tổng quan cho tất cả usecase | Trần Trung Huỳnh   |
| Sửa lại biểu đồ usecase, biểu đồ trình tự, class tổng quan cho uc soát vé vào/ra 24h                          | Trần Thị Thu Hương |
| Vẽ biểu đồ trình tự và class tổng quan cho usecase soát thẻ vào/ra, chỉnh sửa document                        | Trần Quang Huy     |
| Sửa lại biểu đồ usecase                                                                                       | Loy Kakda          |

## Review

| Người review       | Người được review  |
| ------------------ | ------------------ |
| Trần Trung Huỳnh   | Trần Quang Huy     |
| Trần Quang Huy     | Loy Kakda          |
| Loy Kakda          | Trần Thị Thu Hương |
| Trần Thị Thu Hương | Trần Trung Huỳnh   |
