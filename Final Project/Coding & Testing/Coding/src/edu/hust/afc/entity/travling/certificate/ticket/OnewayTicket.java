/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.travling.certificate.ticket;

import edu.hust.afc.entity.station.Station;
import edu.hust.afc.utils.CheckingType;
import edu.hust.afc.utils.Messages;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OnewayTicket extends Ticket {
    /**
     * Balance of one-way ticket
     */
    private float balance;
    /**
     * First station
     */
    private Station firstStation;
    /**
     * Second station
     */
    private Station secondStation;

    public OnewayTicket(String id, String status, Station firstStation, Station secondStation) {
        super(id, status);
        this.firstStation = firstStation;
        this.secondStation = secondStation;
    }

    public OnewayTicket(String id, String status, float balance, Station firstStation, Station secondStation,
                        String barCode) {
        super(id, status, barCode);
        this.balance = balance;
        this.firstStation = firstStation;
        this.secondStation = secondStation;
    }

    public OnewayTicket(ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.balance = resultSet.getFloat("balance");
        this.firstStation = null;
        this.secondStation = null;
    }

    /**
     * Get first station info
     *
     * @return Station
     */
    public Station getFirstStation() {
        return this.firstStation;
    }

    /**
     * Set first station info
     *
     * @param station
     */
    public void setFirstStation(Station station) {
        this.firstStation = station;
    }

    /**
     * Get second station info
     *
     * @return Station
     */
    public Station getSecondStation() {
        return this.secondStation;
    }

    /**
     * Set info of second station
     *
     * @param station
     */
    public void setSecondStation(Station station) {
        this.secondStation = station;
    }

    /**
     * Get balance of one-way ticket
     *
     * @return float
     */
    public float getBalance() {
        return this.balance;
    }

    @Override
    public String toInfo() {
        return "\nType: One-way ticket\nID: " + this.id + "\nBalance: " + this.balance + " euros\n";
    }

    @Override
    public String toString() {
        return barCode + ": One-way ticket between " + firstStation.getName() + " and " + secondStation.getName() + ": "
                + status + " - " + balance + " euros";
    }

    @Override
    public String errorStatusMessage(String action) {
        if (isDestroyed()) {
            return Messages.Ticket.ERROR_DESTROYED_TICKET;
        }

        if (CheckingType.CHECK_IN.equals(action) && isInStation()) {
            return Messages.Ticket.ERROR_IN_USE_TICKET;
        }

        if (CheckingType.CHECK_OUT.equals(action) && isNew()) {
            return Messages.Ticket.ERROR_NEW_TICKET;
        }

        return null;
    }

    /**
     * Return an error if has error and null if has not error
     *
     * @param currentStation : Station where user exit or enter
     * @return String
     */
    public String errorPlatformAreaMessage(Station currentStation) {
        boolean isValidEnterPlatformArea = (currentStation.getDistanceToOrigin() - firstStation.getDistanceToOrigin())
                * (currentStation.getDistanceToOrigin() - secondStation.getDistanceToOrigin()) <= 0;
        if (!isValidEnterPlatformArea) {
            return Messages.Ticket.ERROR_CAN_NOT_ENTER_STATION;
        }
        return null;
    }
}
