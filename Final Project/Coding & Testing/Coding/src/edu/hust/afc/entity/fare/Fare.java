/**
 * @author Tran Quang Huy
 * @version 1
 * @since 07/12/2019
 */
package edu.hust.afc.entity.fare;

import edu.hust.afc.entity.station.Station;

public abstract class Fare {
	/**
	 * enter station
	 */
    protected Station enterStation;
	/**
	 * exit station
	 */
    protected Station exitStation;

    protected Fare() {
    }

    protected Fare(Station enterStation, Station exitStation) {
        this.enterStation = enterStation;
        this.exitStation = exitStation;
    }
    
    /**
     * set entered station
     *
     * @param enterStation
     */
    public Fare setEnterStation(Station enterStation) {
        this.enterStation = enterStation;
        return this;
    }
    /**
     * set exit station
     * 
     * @param exitStation
     */
    public Fare setExitStation(Station exitStation) {
        this.exitStation = exitStation;
        return this;
    }

    /**
     * get fare base on entered station and exit station
     */
    public abstract float getFare();
}
