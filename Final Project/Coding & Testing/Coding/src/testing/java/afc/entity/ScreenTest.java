package testing.java.afc.entity;

import edu.hust.afc.boundary.Screen;
import edu.hust.afc.controller.AFCController;
import edu.hust.afc.entity.station.Station;
import hust.soict.se.customexception.InvalidIDException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ScreenTest {

    @InjectMocks
    private Screen screen;

    @Mock
    private AFCController afcController;

    private final List<Station> stations = new ArrayList<>();

    private final OutputStream outputStream = new ByteArrayOutputStream();

    @Before
    public void setUp() throws Exception {
        stations.add(new Station(1, "Saint-Lazare", 0));
        stations.add(new Station(2, "Madeleine", 5));
        stations.add(new Station(3, "Pyramides", 8.5f));
        stations.add(new Station(4, "Chatelet", 11.3f));
        stations.add(new Station(5, "Gare de Lyon", 15.8f));
        stations.add(new Station(6, "Bercy", 18.9f));
        stations.add(new Station(7, "Cour Saint-Emilion", 22));
        stations.add(new Station(8, "Bibliotheque Francois Mitterrand", 25.3f));
        stations.add(new Station(9, "Olympiades", 28.8f));
        System.setOut(new PrintStream(outputStream));
    }

    @Test
    public void displayStations() {
        when(afcController.getStations()).thenReturn(stations);
        String expected = "------------------------------------------------------------------------------" +
                "There are stations in the line M14 of Paris: " +
                "a. Saint-Lazare" +
                "b. Madeleine" +
                "c. Pyramides" +
                "d. Chatelet" +
                "e. Gare de Lyon" +
                "f. Bercy" +
                "g. Cour Saint-Emilion" +
                "h. Bibliotheque Francois Mitterrand" +
                "i. Olympiades" +
                "Available actions: 1-enter station, 2-exit station" +
                "You can provide a combination of number (1 or 2) and a letter from (a to i) to enter or exit a station (using hyphen in between). For instance, the combination \"2-d\" will bring you to exit the station Chatelet." +
                "Input station you want to enter/exit: " +
                "Your input: 1-d";
        try {
            System.setIn(new ByteArrayInputStream("1-d".getBytes()));
            screen.displayStations();
            String actual = outputStream.toString().replaceAll("\n", "").replaceAll("\r", "");
            Assert.assertEquals(expected, actual);
        } catch (InvalidIDException e) {
            Assert.fail();
        }
    }

    @Test
    public void displayAllTicketsAndCards() {
        String expected = "-------------------------------------------------------------------------------" +
                "These are existing tickets/card:" +
                "Please provide the ticket/card code you want to enter/exit: " +
                "Your input: kdifornd" +
                "------------------------------------------------------------------------------";
        try {
            when(afcController.getTicketOrCards()).thenReturn(Collections.emptyList());
            System.setIn(new ByteArrayInputStream("kdifornd".getBytes()));

            screen.displayAllTicketsAndCards();
            String actual = outputStream.toString().replaceAll("\n", "").replaceAll("\r", "");
            Assert.assertEquals(expected, actual);
        } catch (InvalidIDException e) {
            Assert.fail();
        }
    }

    @After
    public void restore() {
        System.setOut(new PrintStream(System.out));
        System.setIn(System.in);
    }
}