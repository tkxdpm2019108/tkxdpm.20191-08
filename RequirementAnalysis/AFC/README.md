# Máy soát vé tự động (AFC)

## Phân công công việc

| Công việc                                                                                                                                                        | Người thực hiện    |
| ---------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------ |
| Làm usecase tổng quan, từ điển thuật ngữ, đặc tả phụ trợ, biểu đồ hoạt động cho usecase soát vé 1 chiều vào/ra , đặc tả bằng bảng usecase soát vé 1 chiều vào/ra | Trần Trung Huỳnh   |
| Làm usecase tổng quan, Vẽ biểu đồ hoạt động của usecase soát vé, đặc tả usecase soát vé                                                                          | Trần Thị Thu Hương |
| Làm usecase tổng quan, biểu đồ hoạt động cho usecase soát thẻ vào/ra                                                                                             | Trần Quang Huy     |
| Làm usecase tổng quan,                                                                                                                                           | Loy Kakda          |

## Review

| Người review       | Người được review  |
| ------------------ | ------------------ |
| Trần Trung Huỳnh   | Trần Quang Huy     |
| Trần Quang Huy     | Loy Kakda          |
| Loy Kakda          | Trần Thị Thu Hương |
| Trần Thị Thu Hương | Trần Trung Huỳnh   |
