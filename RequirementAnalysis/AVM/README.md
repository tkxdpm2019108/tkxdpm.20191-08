# Máy bán vé tự động (AVM)

## Phân công công việc

| Công việc                                            | Người thực hiện    |
| ---------------------------------------------------- | ------------------ |
| Làm usecase tổng quan, Vẽ biểu đồ hoạt động nạp tiền | Trần Trung Huỳnh   |
| Làm usecase tổng quan, Đặc tả usecase mua vé         | Trần Thị Thu Hương |
| Làm usecase tổng quan, Vẽ biểu đồ usecase mua vé     | Trần Quang Huy     |
| Làm usecase tổng quan,                               | Loy Kakda          |

## Review

| Người review       | Người được review  |
| ------------------ | ------------------ |
| Trần Trung Huỳnh   | Trần Quang Huy     |
| Trần Quang Huy     | Loy Kakda          |
| Loy Kakda          | Trần Thị Thu Hương |
| Trần Thị Thu Hương | Trần Trung Huỳnh   |
