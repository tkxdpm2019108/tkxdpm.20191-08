## Phân công công việc

| Công việc                                                                      | Người thực hiện    |
| ------------------------------------------------------------------------------ | ------------------ |
| Làm màn hình Main, danh sách vé khả dụng, usecase vào/rời ga với vé một chiều  | Trần Trung Huỳnh   |
| Làm màn hình danh sách vé khả dụng, usecase vào/rời ga với vé 24h              | Trần Thị Thu Hương |
| Làm màn hình Main, danh sách vé khả dụng, usecase vào/rời ga với thẻ           | Trần Quang Huy     |
| Làm màn hình Main, thực hiện đọc code từ file bar_codes                        | Loy Kakda          |

| Công việc                                                                      | Người thực hiện    |
| ------------------------------------------------------------------------------ | ------------------ |
| Viết test usecase vào/rời ga với vé một chiều                                  | Trần Trung Huỳnh   |
| Viết test usecase vào/rời ga với vé 24h                                        | Trần Thị Thu Hương |
| Viết test usecase vào/rời ga với thẻ                                           | Trần Quang Huy     |
| Viết test usecase hiện danh sách ga, xử lý bar code                            | Loy Kakda          |

## Review

| Người review       | Người được review  |
| ------------------ | ------------------ |
| Trần Trung Huỳnh   | Trần Quang Huy     |
| Trần Quang Huy     | Loy Kakda          |
| Loy Kakda          | Trần Thị Thu Hương |
| Trần Thị Thu Hương | Trần Trung Huỳnh   |
