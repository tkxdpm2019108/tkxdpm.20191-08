package testing.java.afc.controller;

import edu.hust.afc.controller.PrepaidCardController;
import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingCardDAO;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;
import edu.hust.afc.observer.Subject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PrepaidCardControllerTest {

    @InjectMocks
    private PrepaidCardController cardController;

    @Mock
    private CertificateDAO<PrepaidCard> cardDAO;

    @Mock
    private Subject<String> dataShowing;

    @Mock
    private DealingCardDAO dealingCardDAO;

    @Mock
    private Subject<Boolean> openingGateStatus;

    private List<String> fakeCertificateIds = new ArrayList<>();
    private PrepaidCard fakeNewPrepaidCard;
    private PrepaidCard fakeInStationCard;
    private PrepaidCard fakeInStationCard2;
    private Station fakeStation1;
    private Station fakeStation2;
    private List<PrepaidCard> fakePrepaidCards = new ArrayList<>();

    @Before
    public void init() {
        fakeCertificateIds.add("PC201907160003");
        fakeCertificateIds.add("PC201904120002");
        fakeCertificateIds.add("PC201903020001");
        fakeStation1 = new Station(3, "Pyramides", 8.5f);
        fakeStation2 = new Station(4, "Chatelet", 11.3f);

        fakeNewPrepaidCard = new PrepaidCard("MNBVCXZA", "PC201907160003", 18.5f, null, null);
        fakeInStationCard = new PrepaidCard("PTOUGMNO", "PC201904120002", 0f, "IN", fakeStation1);
        fakeInStationCard2 = new PrepaidCard("ODURYMGK", "PC201903020001", 15f, "IN", fakeStation1);

        fakePrepaidCards.add(fakeNewPrepaidCard);
        fakePrepaidCards.add(fakeInStationCard);
        fakePrepaidCards.add(fakeInStationCard2);
    }

    @Test
    public void getCertificatesByIds_returnListCards() {
        when(cardDAO.getDataByIds(fakeCertificateIds)).thenReturn(fakePrepaidCards);

        List<PrepaidCard> expected = fakePrepaidCards;
        List<PrepaidCard> actual = cardController.getCertificatesByIds(fakeCertificateIds);
        Assert.assertNotNull(actual);
        Assert.assertNotEquals(0, actual.size());
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCertificatesByIds_returnEmpty() {
        when(cardDAO.getDataByIds(Collections.emptyList())).thenReturn(Collections.emptyList());
        List<PrepaidCard> actual = cardController.getCertificatesByIds(Collections.emptyList());
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.size());
    }

    @Test
    public void handleCheckOut() {
        Whitebox.setInternalState(cardController, "prepaidCards", fakePrepaidCards);
        Assert.assertFalse(cardController.handleCheckOut("MNBVCXZA", fakeStation1));
        Assert.assertFalse(cardController.handleCheckOut("PTOUGMNO", fakeStation2));
        Assert.assertTrue(cardController.handleCheckOut("ODURYMGK", fakeStation2));
    }

    @Test
    public void handleCheckIn() {
        Whitebox.setInternalState(cardController, "prepaidCards", fakePrepaidCards);
        Assert.assertTrue(cardController.handleCheckIn("MNBVCXZA", fakeStation1));
        Assert.assertFalse(cardController.handleCheckIn("PTOUGMNO", fakeStation2));
        Assert.assertFalse(cardController.handleCheckIn("ODURYMGK", fakeStation2));
    }
}
