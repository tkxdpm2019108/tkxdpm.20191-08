package testing.java.afc.controller;

import edu.hust.afc.controller.DayTicketController;
import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingDayTicketDAO;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.ticket.DayTicket;
import edu.hust.afc.observer.Subject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.reflect.Whitebox;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DayTicketControllerTest {

    @InjectMocks
    private DayTicketController dayTicketController;

    @Mock
    private CertificateDAO<DayTicket> dayTicketDAO;

    @Mock
    private Subject<String> dataShowing;

    @Mock
    private DealingDayTicketDAO dealingDayTicketDAO;

    @Mock
    private Subject<Boolean> openingGateStatus;

    private List<String> fakeCertificateIds = new ArrayList<>();
    private DayTicket fakeDayTicketNew;
    private DayTicket fakeDayTicketInStation;
    private DayTicket fakeDayTicketOutStation;
    private DayTicket fakeDayTicketDestroyed;
    private Station fakeStation1;
    private Station fakeStation2;

    private List<DayTicket> fakeDayTickets = new ArrayList<>();

    @Before
    public void init() {

        fakeCertificateIds.add("TF201902010001");
        fakeStation1 = new Station(2, "Madeleine", 5f);
        fakeStation2 = new Station(8, "Bercy", 18.9f);

        fakeDayTicketNew = new DayTicket("TF201902010001", "New", "oepwlguc", 0);
        fakeDayTicketInStation = new DayTicket("TF201905060000", "In station", "kdifornd", 1572136223000L);
        fakeDayTicketDestroyed = new DayTicket("TF201906130003", "Destroyed", "lkjhgfds", 1572568223000L);
        fakeDayTicketOutStation = new DayTicket("TF201909150002", "Out station", "oplmdrun", 1572568223000L);

        fakeDayTickets.add(fakeDayTicketNew);
        fakeDayTickets.add(fakeDayTicketInStation);
        fakeDayTickets.add(fakeDayTicketOutStation);
        fakeDayTickets.add(fakeDayTicketDestroyed);
    }

    @Test
    public void getCertificatesByIds_returnListDayTickets() {
        when(dayTicketDAO.getDataByIds(fakeCertificateIds)).thenReturn(fakeDayTickets);

        List<DayTicket> expected = fakeDayTickets;
        List<DayTicket> actual = dayTicketController.getCertificatesByIds(fakeCertificateIds);
        Assert.assertNotNull(actual);
        Assert.assertNotEquals(0, actual.size());
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getCertificatesByIds_returnEmpty() {
        when(dayTicketDAO.getDataByIds(Collections.emptyList())).thenReturn(Collections.emptyList());

        List<DayTicket> actual = dayTicketController.getCertificatesByIds(Collections.emptyList());
        Assert.assertNotNull(actual);
        Assert.assertEquals(0, actual.size());
    }

    @Test
    public void handleCheckOut() {
        Whitebox.setInternalState(dayTicketController, "dayTickets", fakeDayTickets);
        Assert.assertFalse(dayTicketController.handleCheckOut("oplmdrun", fakeStation2));
        Assert.assertFalse(dayTicketController.handleCheckOut("oepwlguc", fakeStation2));
        Assert.assertTrue(dayTicketController.handleCheckOut("kdifornd", fakeStation2));
    }

    @Test
    public void handleCheckIn() {
        Whitebox.setInternalState(dayTicketController, "dayTickets", fakeDayTickets);
        Assert.assertFalse(dayTicketController.handleCheckIn(fakeDayTicketInStation.getBarCode(), fakeStation1));
        Assert.assertFalse(dayTicketController.handleCheckIn(fakeDayTicketDestroyed.getBarCode(), fakeStation1));
        Assert.assertFalse(dayTicketController.handleCheckIn(fakeDayTicketOutStation.getBarCode(), fakeStation1));
        Assert.assertTrue(dayTicketController.handleCheckIn(fakeDayTicketNew.getBarCode(), fakeStation1));
    }
}
