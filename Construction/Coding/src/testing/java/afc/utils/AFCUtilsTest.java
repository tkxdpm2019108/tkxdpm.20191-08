package testing.java.afc.utils;

import org.junit.Assert;
import org.junit.Test;

import edu.hust.afc.utils.AFCUtils;

public class AFCUtilsTest {

    @Test
    public void isCardBarCode_allUpperCaseCharacter_returnTrue() {
        final boolean expected = true;
        final boolean acutal = AFCUtils.isCardBarCode("ABCDEFGH");
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void isCardBarCode_someLowerCaseCharacter_returnFalse() {
        final boolean expected = false;
        final boolean acutal = AFCUtils.isCardBarCode("ABCDefGH");
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void isCardBarCode_allLowerCaseCharacter_returnFalse() {
        final boolean expected = false;
        final boolean acutal = AFCUtils.isCardBarCode("abcefgh");
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void isTicketBarCode_allUpperCaseCharacter_returnFalse() {
        final boolean expected = false;
        final boolean acutal = AFCUtils.isTicketBarCode("ABCDEFGH");
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void isTicketdBarCode_someLowerCaseCharacter_returnFalse() {
        final boolean expected = false;
        final boolean acutal = AFCUtils.isTicketBarCode("ABCDefGH");
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void isTicketBarCode_allLowerCaseCharacter_returnTrue() {
        final boolean expected = true;
        final boolean acutal = AFCUtils.isTicketBarCode("abcefgh");
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void getTime_returnTimeDisplay() {
        final String expected = "09:00 - 06/11/2019";
        final String acutal = AFCUtils.getTime(1573005600000L);
        Assert.assertEquals(expected, acutal);
    }

    @Test
    public void getTime_navigativeMillis() {
        final String expected = "06:59 - 01/01/1970";
        final String acutal = AFCUtils.getTime(-1000);
        Assert.assertEquals(expected, acutal);
    }
}
