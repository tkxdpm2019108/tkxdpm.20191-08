/**
 * Class of mutual handle functions
 *
 * @author Loy Kakda
 * @Version 1.0
 * @since 08/11/2019
 */
package edu.hust.afc.utils;

import hust.soict.se.gate.Gate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class AFCUtils {

    private static final String TIME_FORMAT = "HH:mm - dd/MM/yyyy";
    private static final long ONE_DAY_MILLIS = 86400000L;
    private static final long DELAY_GATE_CLOSING = 5000;

    /**
     * Determine bar code is card's bar code
     *
     * @param barCode : bar code
     * @return bar code is card's bar code or not
     */
    public static boolean isCardBarCode(String barCode) {
        return barCode.toUpperCase(Locale.getDefault()).equals(barCode);
    }

    /**
     * Determine bar code is ticket's bar code
     *
     * @param barCode : bar code
     * @return bar code is ticket's bar code or not
     */
    public static boolean isTicketBarCode(String barCode) {
        return barCode.toLowerCase(Locale.getDefault()).equals(barCode);
    }

    /**
     * Get time from millis value
     *
     * @param millis : bar code
     * @return time
     */
    public static String getTime(long millis) {
        DateFormat dateFormat = new SimpleDateFormat(TIME_FORMAT);
        return dateFormat.format(new Date(millis));
    }

    /**
     * Get expired date of ticket 24h in first using time
     *
     * @return expired date
     */
    public static long getExpireFromCurrent() {
        return System.currentTimeMillis() + ONE_DAY_MILLIS;
    }

    /**
     * Execute open and close gate
     */
    public static void executeGateAutomatically() {
        Gate.getInstance().open();
        try {
			Thread.sleep(DELAY_GATE_CLOSING);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
        Gate.getInstance().close();
    }
}
