/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.travling.certificate.card;

import com.sun.istack.NotNull;
import edu.hust.afc.entity.station.Station;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PrepaidCard extends Card {

    /**
     * Entered station
     */
    private Station enterStation;

    public PrepaidCard(String barCode
            , String id, float balance, String status, Station enterStation) {
        super(barCode, id, balance, status);
        this.enterStation = enterStation;
    }

    public PrepaidCard(@NotNull ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.enterStation = null;
    }

    @Override
    public String toString() {
        return barCode + ": Prepaid card: " + balance + " euros";
    }

    /**
     * Get card info
     */
    public String toInfo() {
        return "Prepaid Card - ID: " + id + " - Balance: " + balance + " euros";
    }
    /**
     * get enter station of PrepaidCard
     * 
     * @return enterStation
     */
    public Station getEnterStation() {
        return enterStation;
    }
    /**
     * Set enter station of PrepaidCard
     *
     * @param enterStation
     */
    public PrepaidCard setEnterStation(Station enterStation) {
        this.enterStation = enterStation;
        return this;
    }
}
