/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */

package edu.hust.afc.entity.travling.certificate.ticket;

import com.sun.istack.NotNull;
import edu.hust.afc.entity.travling.certificate.TravelingCertificate;

import java.sql.ResultSet;
import java.sql.SQLException;

import static edu.hust.afc.entity.code.Code.DAY_TICKET;
import static edu.hust.afc.entity.code.Code.ONE_WAY_TICKET;

public abstract class Ticket extends TravelingCertificate {
    /**
     * Status of ticket when it has not used yet
     */
    public static final String NEW = "New";
    /**
     * Status of ticket when user used ticket and user is being in station
     */
    public static final String IN_STATION = "In station";
    /**
     * Status of ticket when user out of station
     */
    public static final String OUT_STATION = "Out station";
    /**
     * Status of ticket when it has used
     */
    public static final String DESTROYED = "Destroyed";

    /**
     * status current of ticket
     */
    protected String status;

    public Ticket(String id, String status) {
        super(id, null);
        this.status = status;
    }

    public Ticket(String id, String status, String barCode) {
        super(id, barCode);
        this.status = status;
    }

    public Ticket(@NotNull ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.status = resultSet.getString("status");
    }

    /**
     * Check ticket is one-way ticket or not
     *
     * @param ticketId - id of ticket
     * @return boolean
     */
    public static boolean isOneWayTicketId(@NotNull String ticketId) {
        return ONE_WAY_TICKET.equals(ticketId.substring(0, 2));
    }

    /**
     * Check ticketIs is DayTicketId or not
     *
     * @param ticketId
     * @return boolean
     */
    public static boolean isDayTicketId(@NotNull String ticketId) {
        return DAY_TICKET.equals(ticketId.substring(0, 2));
    }

    /**
     * Get status of ticket
     *
     * @return String
     */
    public String getStatus() {
        return status;
    }

    /**
     * Set status of ticket
     *
     * @param status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Format info of ticket for displaying
     *
     * @return String
     */
    public abstract String toInfo();

    /**
     * Return an error if has error and null if has not error
     *
     * @param action - string of action exit/enter
     * @return String
     */
    public abstract String errorStatusMessage(String action);

    /**
     * Check status of ticket is new or not
     *
     * @return boolean
     */
    public boolean isNew() {
        return NEW.equals(status);
    }

    /**
     * Check status of ticket is destroyed or not
     *
     * @return boolean
     */
    public boolean isDestroyed() {
        return DESTROYED.equals(status);
    }

    /**
     * Check status of ticket is in station or not
     *
     * @return boolean
     */
    public boolean isInStation() {
        return IN_STATION.equals(status);
    }
}
