/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.code;

import com.sun.istack.NotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Code {
	/**
	 * type is onewayTicket
	 */
    public static final String ONE_WAY_TICKET = "OW";
	/**
	 * type is dayTicket
	 */
    public static final String DAY_TICKET = "TF";
	/**
	 * type is prepaidCard
	 */
    public static final String PREPAID_CARD = "PC";
	/**
	 * code of card/ticket
	 */
    private String code;
	/**
	 * type
	 */
    private String type;
	/**
	 * certificate id of card/ticket
	 */
    private String certificateId;

    public Code(String code, String type, String certificateId) {
        this.code = code;
        this.type = type;
        this.certificateId = certificateId;
    }
    
	/**
	 * Constructor of code card/ticket
	 * 
	 * @param resultSet
	 * @throws SQLException if wrong
	 */
    public Code(@NotNull ResultSet resultSet) throws SQLException {
        this.code = resultSet.getString(resultSet.findColumn("id"));
        this.type = resultSet.getString(resultSet.findColumn("type"));
        this.certificateId = resultSet.getString(resultSet.findColumn("id_ticket_or_card"));
    }
    /**
	 * Get code of card/ticket
	 * 
	 * @return code
	 */
    public String getCode() {
        return code;
    }
    /**
	 * Get type 
	 * 
	 * @return type
	 */
    public String getType() {
        return type;
    }
    /**
	 * Get certificate Id of card/ticket
	 * 
	 * @return certificateId
	 */
    public String getCertificateId() {
        return certificateId;
    }
}
