/**
 * Control logic, execute data about prepaid card
 * Handle event check in or check out with prepaid card
 *
 * @author Tran Quang Huy
 * @version 1
 * @since 08-11-2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.db.dao.*;
import edu.hust.afc.entity.code.Code;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;
import edu.hust.afc.entity.travling.certificate.ticket.DayTicket;
import edu.hust.afc.entity.travling.certificate.ticket.OnewayTicket;
import edu.hust.afc.observer.Subject;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.CheckingType;
import hust.soict.se.customexception.InvalidIDException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AFCController {
    /**
     * Data to display to screen
     */
    private Subject<String> dataDisplay = new Subject<>();
    
    /**
     * Control logic and data about code
     */
    private CodeController codeController;
    /**
     * Control logic and data about card
     */
    private CertificateController<PrepaidCard> cardController;
    /**
     * Control logic and data about one way ticket
     */
    private CertificateController<OnewayTicket> onewayTicketController;
    /**
     * Control logic and data about 24h ticket
     */
    private CertificateController<DayTicket> dayTicketController;

    /**
     * Control logic and data about station
     */
    private StationController stationController;

    /**
     * Action of user (Check in/Check out)
     */
    private String action = CheckingType.CHECK_IN;
    /**
     * Station when user check in or check out
     */
    private Station station = null;

    public AFCController() {
        initComponents();
    }

    /**
     * init components : dao, controller, database connection, ...
     */
    private void initComponents() {
        DBConnection dbConnection = DBConnection.getInstance();

        StationDAO stationDAO = new StationDAOImpl(dbConnection);
        CodeDAO codeDAO = new CodeDAOImpl(dbConnection);
        CertificateDAO<PrepaidCard> cardDAO = new CardDAOImpl(dbConnection);
        CertificateDAO<OnewayTicket> onewayTicketDAO = new OnewayTicketDAOImpl(dbConnection);
        CertificateDAO<DayTicket> dayTicketDAO = new DayTicketDAOImpl(dbConnection);

        DealingCardDAO dealingCardDAO = new DealingCardDAOImpl(dbConnection);
        DealingOnewayTicketDAO dealingOnewayTicketDAO = new DealingOnewayTicketDAOImpl(dbConnection);
        DealingDayTicketDAO dealingDayTicketDAO = new DealingDayTicketDAOImpl(dbConnection);

        stationController = new StationController(stationDAO);
        codeController = new CodeController(codeDAO);
        cardController = new PrepaidCardController(cardDAO, dealingCardDAO, dataDisplay);
        onewayTicketController = new OnewayTicketController(onewayTicketDAO, dealingOnewayTicketDAO, dataDisplay);
        dayTicketController = new DayTicketController(dayTicketDAO, dealingDayTicketDAO, dataDisplay);
    }

    /**
     * Get all station's information
     *
     * @return list of stations
     */
    public List<Station> getStations() {
        return stationController.getStations();
    }

    /**
     * handle input about station
     *
     * @param input : input of user
     * @throws InvalidIDException if wrong input
     */
    public void handleStationInput(String input) throws InvalidIDException {
        try {
            action = input.split("-")[0];
            char stationId = input.split("-")[1].charAt(0);
            station = stationController.getStation(stationId);
        } catch (Exception e) {
            throw new InvalidIDException();
        }
    }

    /**
     * Get tickets or cards
     *
     * @return list of tickets/cards information
     * @throws InvalidIDException if wrong input
     */
    public List<String> getTicketOrCards() throws InvalidIDException {
        List<String> certificateIds = codeController.getTicketsOrCardIds();

        List<PrepaidCard> prepaidCards = cardController.getCertificatesByIds(certificateIds);
        List<OnewayTicket> onewayTickets = onewayTicketController.getCertificatesByIds(certificateIds);
        List<DayTicket> dayTickets = dayTicketController.getCertificatesByIds(certificateIds);

        List<String> certificates = new ArrayList<>();
        certificates.addAll(onewayTickets.stream().map(OnewayTicket::toString).collect(Collectors.toList()));
        certificates.addAll(dayTickets.stream().map(DayTicket::toString).collect(Collectors.toList()));
        certificates.addAll(prepaidCards.stream().map(PrepaidCard::toString).collect(Collectors.toList()));

        return certificates;
    }

    /**
     * Handle bar code input
     *
     * @param barCodeInput bar code input
     * @throws InvalidIDException if wrong input
     */
    public void handleBarCodeInput(String barCodeInput) throws InvalidIDException {
        String codeType;
        boolean isValid;
        try {
            codeType = codeController.getCodeType(barCodeInput);
            isValid = CheckingType.CHECK_IN.equals(action)
                    ? validateCheckIn(codeType, barCodeInput)
                    : validateCheckOut(codeType, barCodeInput);
        } catch (Exception exception) {
            throw new InvalidIDException();
        }
        if (isValid) {
            AFCUtils.executeGateAutomatically();
        }
    }
    /**
     * validate checkin
     *
     * @param codeType : code type, barCodeInput : bar code input
     * @return handle card/ticket checking in
     */

    private boolean validateCheckIn(String codeType, String barCodeInput) {
        switch (codeType) {
            case Code.PREPAID_CARD:
                return cardController.handleCheckIn(barCodeInput, station);
            case Code.ONE_WAY_TICKET:
                return onewayTicketController.handleCheckIn(barCodeInput, station);
            case Code.DAY_TICKET:
                return dayTicketController.handleCheckIn(barCodeInput, station);
            default:
                return false;
        }
    }
    /**
     * validate checkout
     *
     * @param codeType : code type, barCodeInput : bar code input
     * @return handle card/ticket checking in
     */
    private boolean validateCheckOut(String codeType, String barCodeInput) {
        switch (codeType) {
            case Code.PREPAID_CARD:
                return cardController.handleCheckOut(barCodeInput, station);
            case Code.ONE_WAY_TICKET:
                return onewayTicketController.handleCheckOut(barCodeInput, station);
            case Code.DAY_TICKET:
                return dayTicketController.handleCheckOut(barCodeInput, station);
            default:
                return false;
        }
    }

    /**
     * Get subject which observe about result can print to screen
     *
     * @return subject of string
     */
    public Subject<String> getDataDisplay() {
        return dataDisplay;
    }
}
