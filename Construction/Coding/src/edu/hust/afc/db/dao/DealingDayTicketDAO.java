/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.entity.dealing.DealingDayTicket;

public interface DealingDayTicketDAO {
	
    /**
     * Store a dealing
     *
     * @param dealing dealing information
     * @return successful or failed storing
     */
	
    boolean save(DealingDayTicket dealing);
}
