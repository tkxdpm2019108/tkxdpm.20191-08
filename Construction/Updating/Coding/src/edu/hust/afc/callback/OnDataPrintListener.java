package edu.hust.afc.callback;

public interface OnDataPrintListener {
    void onPrint(String data);
}
