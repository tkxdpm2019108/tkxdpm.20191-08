/**
 * Handle one-way ticket information controlling, check-in, check-out one-way ticket
 *
 * @author Tran Trung Huynh
 * @Version 1.0
 * @since 25/08/2019
 */

package edu.hust.afc.controller;

import com.sun.istack.NotNull;
import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingOnewayTicketDAO;
import edu.hust.afc.entity.dealing.Dealing;
import edu.hust.afc.entity.dealing.DealingOnewayTicket;
import edu.hust.afc.entity.distance.Distance;
import edu.hust.afc.entity.distance.OneLineDistance;
import edu.hust.afc.entity.fare.DistanceFare;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.ticket.OnewayTicket;
import edu.hust.afc.entity.travling.certificate.ticket.Ticket;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.CheckingType;
import edu.hust.afc.utils.Messages;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller
 */
public class OnewayTicketController extends BaseCertificateController<OnewayTicket> {
    /**
     * List of oneway ticket in file
     */
    private List<OnewayTicket> onewayTickets = Collections.emptyList();

    /**
     * instance of DealingOnewayTicketDAO class for get or insert ticket info in
     * database
     */
    private CertificateDAO<OnewayTicket> onewayTicketDAO;

    /**
     * instance of DealingOnewayTicketDAO class for get dealing data in database
     */
    private DealingOnewayTicketDAO dealingOnewayTicketDAO;

    public OnewayTicketController(CertificateDAO<OnewayTicket> onewayTicketDAO, DealingOnewayTicketDAO dealingOnewayTicketDAO) {
        this.onewayTicketDAO = onewayTicketDAO;
        this.dealingOnewayTicketDAO = dealingOnewayTicketDAO;
    }

    @Override
    public List<OnewayTicket> getCertificatesByIds(List<String> certificateIds) {
        List<String> onewayTicketIds = certificateIds.stream().filter(Ticket::isOneWayTicketId)
                .collect(Collectors.toList());
        onewayTickets = onewayTicketDAO.getDataByIds(onewayTicketIds);
        return onewayTickets;
    }

    @Override
    public boolean handleCheckIn(@NotNull String barCodeInput, @NotNull Station station) {
        OnewayTicket onewayTicket = findCertificateByBarCode(onewayTickets, barCodeInput);
        if (onewayTicket == null) return false;

        String errorStatus = onewayTicket.errorStatusMessage(CheckingType.CHECK_IN);
        String errorPlatformArea = onewayTicket.errorPlatformAreaMessage(station);

        if (errorStatus != null) {
            dataToDisplay.notifyChange(onewayTicket.toInfo());
            dataToDisplay.notifyChange(errorStatus);
            return false;
        } else if (errorPlatformArea != null) {
            dataToDisplay.notifyChange(onewayTicket.toInfo());
            dataToDisplay.notifyChange(errorPlatformArea);
            return false;
        }

        onewayTicket.checkIn(station);
        onewayTicketDAO.updateData(onewayTicket);
        dealingOnewayTicketDAO.save(createDealing(onewayTicket, station, Dealing.ACTION_ENTER));

        dataToDisplay.notifyChange(onewayTicket.toInfo());
        dataToDisplay.notifyChange(Messages.Ticket.SUCCESS_VALID_TICKET);
        return true;
    }

    @Override
    public boolean handleCheckOut(@NotNull String barCodeInput, @NotNull Station station) {
        OnewayTicket onewayTicket = findCertificateByBarCode(onewayTickets, barCodeInput);
        if (onewayTicket == null) return false;

        String errorStatus = onewayTicket.errorStatusMessage(CheckingType.CHECK_OUT);

        if (errorStatus != null) {
            dataToDisplay.notifyChange(onewayTicket.toInfo());
            dataToDisplay.notifyChange(errorStatus);
            return false;
        }

        Station enteredStation = dealingOnewayTicketDAO.getStationByOnewayTicketId(onewayTicket.getId());
        float payment = calculateMoney(enteredStation, station);
        if (!onewayTicket.isReadyToPay(payment)) {
            dataToDisplay.notifyChange(onewayTicket.toInfo());
            dataToDisplay.notifyChange(String.format(Messages.ERROR_NOT_ENOUGH_BALANCE, payment));
            return false;
        }

        onewayTicket.checkOut(payment);
        onewayTicketDAO.updateData(onewayTicket);
        dealingOnewayTicketDAO.save(createDealing(onewayTicket, station, Dealing.ACTION_EXIT));

        dataToDisplay.notifyChange(onewayTicket.toInfo());
        dataToDisplay.notifyChange(Messages.Ticket.SUCCESS_VALID_TICKET);
        return true;
    }

    /**
     * create Dealing of DealingOnewayTicket
     *
     * @param ticket
     * @param station
     * @param action
     * @return DealingOnewayTicket
     */
    private DealingOnewayTicket createDealing(Ticket ticket, Station station, String action) {
        String time = AFCUtils.getTime(System.currentTimeMillis());
        return new DealingOnewayTicket(0, time, action, station.getId(), ticket.getId());
    }

    /**
     * calculate money fare
     *
     * @param enterStation
     * @param exitStation
     * @return get fare on entered station and exit station
     */
    private float calculateMoney(Station enterStation, Station exitStation) {
        Distance distance = new OneLineDistance(enterStation, exitStation);
        return new DistanceFare(distance).getFare();
    }
}
