package edu.hust.afc.controller;

import edu.hust.afc.callback.OnDataPrintListener;
import edu.hust.afc.entity.travling.certificate.TravelingCertificate;
import edu.hust.afc.observer.Subject;

import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseCertificateController<T extends TravelingCertificate> implements CertificateController<T> {

    /**
     * receive data to update, and notify to screen to show
     */
    protected Subject<String> dataToDisplay;

    protected OnDataPrintListener printListener;

    public void setPrintListener(OnDataPrintListener printListener) {
        this.printListener = printListener;
    }

    public void setDataToDisplay(Subject<String> dataToDisplay) {
        this.dataToDisplay = dataToDisplay;
    }

    protected T findCertificateByBarCode(List<T> certificates, String barCode) {
        List<T> filteredCertificates = certificates.stream()
                .filter(certificate -> barCode.equals(certificate.getBarCode()))
                .collect(Collectors.toList());
        return filteredCertificates.isEmpty() ? null : filteredCertificates.get(0);
    }
}
