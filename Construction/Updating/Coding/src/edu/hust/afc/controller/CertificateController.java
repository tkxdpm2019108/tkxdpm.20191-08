/**
 * interface for card/ticket controller
 *
 * @author Tran Quang Huy
 * @Version 1.0
 * @since 08/11/2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.TravelingCertificate;

import java.util.List;
import java.util.stream.Collectors;

public interface CertificateController<T extends TravelingCertificate> extends Controller{

    /**
     * Get all ready certificate(type: {@link T}) from list of bar code
     *
     * @param certificateIds list of ready bar code
     *                       we create a list of {@code cardIds} from {@code certificateIds} by use a filter
     * @return list of prepaid cards/tickets
     */
    List<T> getCertificatesByIds(List<String> certificateIds);

    /**
     * Handle card/ticket checking in
     *
     * @param station Current station where card is scanned
     * @return checking is successful or failed
     */
    boolean handleCheckIn(String barCodeInput, Station station);

    /**
     * Handle card/ticket checking out
     *
     * @param station Current station where card is scanned
     * @return checking is successful or failed
     */
    boolean handleCheckOut(String barCodeInput, Station station);

//    default T findCertificateByBarCode(List<T> certificates, String barCode) {
//        List<T> filteredCertificates = certificates.stream()
//                .filter(certificate -> barCode.equals(certificate.getBarCode()))
//                .collect(Collectors.toList());
//        return filteredCertificates.isEmpty() ? null : filteredCertificates.get(0);
//    }
}
