/**
 * Handle 24h ticket's information controlling
 *
 * @author Tran Thi Thu Huong
 * @Version 1.0
 * @since 25/08/2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingDayTicketDAO;
import edu.hust.afc.entity.dealing.Dealing;
import edu.hust.afc.entity.dealing.DealingDayTicket;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.ticket.DayTicket;
import edu.hust.afc.entity.travling.certificate.ticket.Ticket;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.CheckingType;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller
 */
public class DayTicketController extends BaseCertificateController<DayTicket> {
    /**
     * List of day ticket in file
     */
    private List<DayTicket> dayTickets = Collections.emptyList();
    /**
     * instance of DealingDayTicketDAO class for get or insert ticket info in
     * database
     */
    private CertificateDAO<DayTicket> dayTicketDAO;
    /**
     * instance of DealingDayTicketDAO class for get dealing data in database
     */
    private DealingDayTicketDAO dealingDayTicketDAO;

    public DayTicketController(CertificateDAO<DayTicket> dayTicketDAO, DealingDayTicketDAO dealingDayTicketDAO) {
        this.dayTicketDAO = dayTicketDAO;
        this.dealingDayTicketDAO = dealingDayTicketDAO;
    }

    @Override
    public List<DayTicket> getCertificatesByIds(List<String> certificateIds) {
        List<String> dayTicketIds = certificateIds.stream().filter(Ticket::isDayTicketId).collect(Collectors.toList());
        dayTickets = dayTicketDAO.getDataByIds(dayTicketIds);
        return dayTickets;
    }

    @Override
    public boolean handleCheckIn(String barCodeInput, Station station) {
        DayTicket dayTicket = findCertificateByBarCode(dayTickets, barCodeInput);
        if (dayTicket == null) return false;

        String errorStatus = dayTicket.errorStatusMessage(CheckingType.CHECK_IN);

        if (errorStatus != null) {
            dataToDisplay.notifyChange(dayTicket.toInfo());
            dataToDisplay.notifyChange(errorStatus);
            return false;
        }

        dayTicket.checkIn();
        dayTicketDAO.updateData(dayTicket);
        dealingDayTicketDAO.save(createDealing(dayTicket, station, Dealing.ACTION_ENTER));
        dataToDisplay.notifyChange(dayTicket.toInfo());
        return true;
    }

    @Override
    public boolean handleCheckOut(String barCodeInput, Station station) {
        DayTicket dayTicket = findCertificateByBarCode(dayTickets, barCodeInput);
        if (dayTicket == null) return false;

        String errorStatus = dayTicket.errorStatusMessage(CheckingType.CHECK_OUT);

        if (errorStatus != null) {
            dataToDisplay.notifyChange(dayTicket.toInfo());
            dataToDisplay.notifyChange(errorStatus);
            return false;
        }
        dayTicket.checkOut();
        dayTicketDAO.updateData(dayTicket);
        dealingDayTicketDAO.save(createDealing(dayTicket, station, Dealing.ACTION_EXIT));
        dataToDisplay.notifyChange(dayTicket.toInfo());
        return true;
    }

    /**
     * create Dealing of DealingDayTicket
     *
     * @param ticket
     * @param station
     * @param action
     * @return DealingDayTicket
     */
    private DealingDayTicket createDealing(Ticket ticket, Station station, String action) {
        String time = AFCUtils.getTime(System.currentTimeMillis());
        return new DealingDayTicket(0, time, action, station.getId(), ticket.getId());
    }
}
