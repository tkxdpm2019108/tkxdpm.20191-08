/**
 * {@link edu.hust.afc.controller.PrepaidCardController} control logic, execute data about prepaid card
 * Handle event check in or check out with prepaid card
 *
 * @author Tran Quang Huy
 * @version 1
 * @since 27-10-2019
 */

package edu.hust.afc.controller;

import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingCardDAO;
import edu.hust.afc.entity.dealing.Dealing;
import edu.hust.afc.entity.dealing.DealingCard;
import edu.hust.afc.entity.distance.Distance;
import edu.hust.afc.entity.distance.OneLineDistance;
import edu.hust.afc.entity.fare.DistanceFare;
import edu.hust.afc.entity.fare.Fare;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.Messages;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Controller of prepaid Card
 */
public class PrepaidCardController extends BaseCertificateController<PrepaidCard> {

    /**
     * List of card in file
     */
    private List<PrepaidCard> prepaidCards = Collections.emptyList();

    /**
     * Data access object of card
     */
    private CertificateDAO<PrepaidCard> cardDAO;
    /**
     * Data access object of dealing card
     */
    private DealingCardDAO dealingCardDAO;

    public PrepaidCardController(CertificateDAO<PrepaidCard> cardDAO, DealingCardDAO dealingCardDAO) {
        this.cardDAO = cardDAO;
        this.dealingCardDAO = dealingCardDAO;
    }

    @Override
    public List<PrepaidCard> getCertificatesByIds(List<String> certificateIds) {
        List<String> cardIds = certificateIds.stream().filter(PrepaidCard::isCardId).collect(Collectors.toList());
        prepaidCards = cardDAO.getDataByIds(cardIds);
        return prepaidCards;
    }

    @Override
    public boolean handleCheckIn(String barCodeInput, Station station) {
        PrepaidCard prepaidCard = findCertificateByBarCode(prepaidCards, barCodeInput);
        if (prepaidCard == null) return false;

        if (prepaidCard.isInStation()) {
            dataToDisplay.notifyChange(prepaidCard.toInfo());
            dataToDisplay.notifyChange(Messages.Card.ERROR_NEED_CHECK_OUT);
            return false;
        }
        prepaidCard.checkIn(station);
        cardDAO.updateData(prepaidCard);
        dealingCardDAO.save(createDealing(prepaidCard, station, Dealing.ACTION_ENTER));
        dataToDisplay.notifyChange(prepaidCard.toInfo());
        return true;
    }

    @Override
    public boolean handleCheckOut(String barCodeInput, Station station) {
        PrepaidCard prepaidCard = findCertificateByBarCode(prepaidCards, barCodeInput);

        if (prepaidCard == null) return false;

        if (!prepaidCard.isInStation()) {
            printListener.onPrint(prepaidCard.toInfo());
            printListener.onPrint(Messages.Card.ERROR_NEED_CHECK_IN);
//            dataToDisplay.notifyChange(prepaidCard.toInfo());
//            dataToDisplay.notifyChange(Messages.Card.ERROR_NEED_CHECK_IN);
            return false;
        }

        float money = calculateMoney(prepaidCard.getEnterStation(), station);
        if (!prepaidCard.isReadyToPay(money)) {
            dataToDisplay.notifyChange(prepaidCard.toInfo());
            dataToDisplay.notifyChange(String.format(Messages.ERROR_NOT_ENOUGH_BALANCE, money));
            return false;
        }

        prepaidCard.checkOut(money);

        cardDAO.updateData(prepaidCard);
        dealingCardDAO.save(createDealing(prepaidCard, station, Dealing.ACTION_EXIT));
        dataToDisplay.notifyChange(prepaidCard.toInfo());
        return true;
    }

    /**
     * create Dealing of DealingCard
     *
     * @param card
     * @param station
     * @param action
     * @return DealingCard
     */
    private DealingCard createDealing(PrepaidCard card, Station station, String action) {
        String time = AFCUtils.getTime(System.currentTimeMillis());
        return new DealingCard(0, time, action, station.getId(), card.getId());
    }

    /**
     * calculate money fare
     *
     * @param enterStation
     * @param exitStation
     * @return get fare on entered station and exit station
     */
    private float calculateMoney(Station enterStation, Station exitStation) {
        Distance distance = new OneLineDistance(enterStation, exitStation);
        return new DistanceFare(distance).getFare();
    }
}
