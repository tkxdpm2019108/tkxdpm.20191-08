package edu.hust.afc.controller;

import edu.hust.afc.db.dao.*;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;
import edu.hust.afc.entity.travling.certificate.ticket.DayTicket;
import edu.hust.afc.entity.travling.certificate.ticket.OnewayTicket;

public class ControllerFactory {
    public static final String STATION = "StationController";
    public static final String CODE = "CodeController";
    public static final String PREPAID_CARD = "PrepaidCardController";
    public static final String ONE_WAY_TICKET = "OnewayTicketController";
    public static final String DAY_TICKET = "DayTicketController";

    private static ControllerFactory instance;
    private DAOFactory daoFactory;

    private ControllerFactory(DAOFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    public static ControllerFactory getInstance(DAOFactory daoFactory) {
        if (instance == null) {
            instance = new ControllerFactory(daoFactory);
        }
        return instance;
    }

    public Controller getController(String type) {
        if (type == null) return null;

        switch (type) {
            case STATION:
                return new StationController((StationDAO) daoFactory.getDAO(DAOFactory.STATION));
            case CODE:
                return new CodeController((CodeDAO) daoFactory.getDAO(DAOFactory.CODE));
            case PREPAID_CARD:
                CertificateDAO<PrepaidCard> prepaidCardDAO = (PrepaidCardDAOImpl) daoFactory.getDAO(DAOFactory.PREPAID_CARD);
                DealingCardDAO dealingCardDAO = (DealingCardDAO) daoFactory.getDAO(DAOFactory.DEALING_PREPAID_CARD);
                return new PrepaidCardController(prepaidCardDAO, dealingCardDAO);
            case ONE_WAY_TICKET:
                CertificateDAO<OnewayTicket> onewayTicketDAO = (OnewayTicketDAOImpl) daoFactory.getDAO(DAOFactory.ONE_WAY_TICKET);
                DealingOnewayTicketDAO dealingOnewayTicketDAO = (DealingOnewayTicketDAOImpl) daoFactory.getDAO(DAOFactory.DEALING_ONE_WAY_TICKET);
                return new OnewayTicketController(onewayTicketDAO, dealingOnewayTicketDAO);
            case DAY_TICKET:
                CertificateDAO<DayTicket> dayTicketDAO = (DayTicketDAOImpl) daoFactory.getDAO(DAOFactory.DAY_TICKET);
                DealingDayTicketDAO dealingDayTicketDAO = (DealingDayTicketDAOImpl) daoFactory.getDAO(DAOFactory.DEALING_DAY_TICKET);
                return new DayTicketController(dayTicketDAO, dealingDayTicketDAO);
            default:
                return null;
        }
    }
}
