package edu.hust.afc.entity.distance;

import edu.hust.afc.entity.station.Station;
import org.jetbrains.annotations.NotNull;

public class OneLineDistance implements Distance {

    /**
     * enter station
     */
    private Station enterStation;
    /**
     * exit station
     */
    private Station exitStation;

    public OneLineDistance() {
    }

    public OneLineDistance(@NotNull Station enterStation, @NotNull Station exitStation) {
        this.enterStation = enterStation;
        this.exitStation = exitStation;
    }

    @Override
    public float getDistance() {
        return Math.abs(enterStation.getDistanceToOrigin() - exitStation.getDistanceToOrigin());
    }

    /**
     * set entered station
     *
     * @param enterStation
     */
    public OneLineDistance setEnterStation(Station enterStation) {
        this.enterStation = enterStation;
        return this;
    }

    /**
     * set exit station
     *
     * @param exitStation
     */
    public OneLineDistance setExitStation(Station exitStation) {
        this.exitStation = exitStation;
        return this;
    }
}
