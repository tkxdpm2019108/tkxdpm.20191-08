/**
 * @author Tran Quang Huy
 * @version 1
 * @since 23/11/2019
 */
package edu.hust.afc.entity.travling.certificate.card;

import com.sun.istack.NotNull;
import edu.hust.afc.entity.travling.certificate.TravelingCertificate;

import java.sql.ResultSet;
import java.sql.SQLException;

import static edu.hust.afc.entity.code.Code.PREPAID_CARD;

public abstract class Card extends TravelingCertificate {
    /**
     * Status of card when user in of station
     */
    protected static final String CARD_IN_STATION = "IN";
    /**
     * Status of card when user out of station
     */
    protected static final String CARD_OUT_STATION = "OUT";
    /**
     * length card id
     */
    private static final int CARD_ID_LENGTH = 14;

    /**
     * Balance of card
     */
    protected float balance;
    /**
     * Status of card : (In station or out station)
     */
    protected String status;

    /**
     * Constructor of card
     *
     * @param id
     * @param balance
     */
    public Card(String id, float balance) {
        super(id, null);
        this.balance = balance;
    }

    public Card(String barCode, String id, float balance, String status) {
        super(id, barCode);
        this.balance = balance;
        this.status = status;
    }

    /**
     * Constructor of card
     *
     * @param resultSet
     * @throws SQLException if wrong
     */
    public Card(@NotNull ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.barCode = resultSet.getString("bar_code");
        this.balance = resultSet.getFloat("balance");
        this.status = resultSet.getString("status");
    }

    /**
     * Check is card id
     *
     * @param cardId
     * @return boolean
     */
    public static boolean isCardId(@NotNull String cardId) {
        return cardId.length() == CARD_ID_LENGTH && PREPAID_CARD.equals(cardId.substring(0, 2));
    }

    /**
     * Check if card is in station
     *
     * @return boolean
     */
    public boolean isInStation() {
        return CARD_IN_STATION.equals(status);
    }

    @Override
    public boolean isReadyToPay(float money) {
        return money <= balance;
    }
}
