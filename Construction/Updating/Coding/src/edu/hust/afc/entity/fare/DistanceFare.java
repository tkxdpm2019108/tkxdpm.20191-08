/**
 * @author Tran Quang Huy
 * @version 1
 * @since 07/12/2019
 */
package edu.hust.afc.entity.fare;

import edu.hust.afc.entity.distance.Distance;
import org.jetbrains.annotations.NotNull;

public class DistanceFare implements Fare {

    /**
     * base distance
     */
    private static final float BASE_DISTANCE = 5f;
    /**
     * base fare unit
     */
    private static final float BASE_FARE_UNIT = 1.9f;
    /**
     * fare over base unit
     */
    private static final float FARE_OVER_BASE_UNIT = 0.4f;

    private Distance distance;

    public DistanceFare(@NotNull Distance distance) {
        this.distance = distance;
    }

    @Override
    public float getFare() {
        float d = distance.getDistance();
        if (d <= BASE_DISTANCE) return BASE_FARE_UNIT;
        float temp = (d - BASE_DISTANCE) / 2;
        int multiple = temp == (int) temp ? (int) temp : ((int) temp + 1);
        return BASE_FARE_UNIT + multiple * FARE_OVER_BASE_UNIT;
    }
}
