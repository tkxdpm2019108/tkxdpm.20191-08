/**
 * @author Tran Trung Huynh
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.station;


import java.sql.ResultSet;
import java.sql.SQLException;

import com.sun.istack.NotNull;


public class Station {
    /**
     * id of station
     */
    private int id;
    /**
     * name of station
     */
    private String name;
    /**
     * distance to origin of station
     */
    private float distanceToOrigin;
    
	/**
	 * Constructor of station
	 * 
	 * @param id
	 * @param name
	 * @param distanceToOrigin
	 */
    public Station(int id, String name, float distanceToOrigin) {
        this.id = id;
        this.name = name;
        this.distanceToOrigin = distanceToOrigin;
    }
    /**
	 * Constructor of station
	 * 
	 * @param resultSet
	 * @throws SQLException if wrong
	 */
    public Station(@NotNull ResultSet resultSet) throws SQLException {
        this.id = resultSet.getInt("id");
        this.name = resultSet.getString("name");
        this.distanceToOrigin = resultSet.getFloat("distanceToOrigin");
    }

    @Override
    public String toString() {
        return (char) (96 + id) + ". " + name;
    }
    /**
     * get id of station
     * 
     * @return int
     */
    public int getId() {
        return id;
    }

    /**
     * get name of station
     * 
     * @return String
     */
    public String getName() {
        return name;
    }
    /**
     * get distance to origin of station
     * 
     * @return float
     */
    public float getDistanceToOrigin() {
        return distanceToOrigin;
    }
}
