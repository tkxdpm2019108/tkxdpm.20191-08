/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.travling.certificate.card;

import com.sun.istack.NotNull;
import edu.hust.afc.entity.station.Station;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PrepaidCard extends Card {

    /**
     * Entered station
     */
    private Station enterStation;

    public PrepaidCard(String barCode, String id, float balance, String status, Station enterStation) {
        super(barCode, id, balance, status);
        this.enterStation = enterStation;
    }

    public PrepaidCard(@NotNull ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.enterStation = null;
    }

    @Override
    public void checkIn(Station station) {
        status = CARD_IN_STATION;
        enterStation = station;
    }

    @Override
    public void checkOut(float fare) {
        status = CARD_OUT_STATION;
        enterStation = null;
        balance -= fare;
    }

    @Override
    public String toString() {
        return barCode + ": Prepaid card: " + balance + " euros";
    }

    /**
     * Get card info
     */
    public String toInfo() {
        return "Prepaid Card - ID: " + id + " - Balance: " + balance + " euros";
    }

    /**
     * get enter station of PrepaidCard
     *
     * @return enterStation
     */
    public Station getEnterStation() {
        return enterStation;
    }

    public void setEnterStation(Station enterStation) {
        this.enterStation = enterStation;
    }

    public String getUpdateQuery(String formatString) {
        int enterStationId = enterStation == null ? 0 : enterStation.getId();
        return String.format(formatString, balance, status, enterStationId, barCode, id);
    }
}
