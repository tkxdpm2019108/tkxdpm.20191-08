/**
 * @author Tran Thi Thu Huong
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.entity.travling.certificate.ticket;

import edu.hust.afc.entity.station.Station;
import edu.hust.afc.utils.AFCUtils;
import edu.hust.afc.utils.CheckingType;
import edu.hust.afc.utils.Messages;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DayTicket extends Ticket {
    /**
     * seconds of one day
     */
    private static final long ONE_DAY_MILLIS = 86400000L;
    /**
     * expire of day ticket
     */
    private long expire;

    public DayTicket(String id, String status, String barCode, long expire) {
        super(id, status, barCode);
        this.expire = expire;
    }

    public DayTicket(ResultSet resultSet) throws SQLException {
        super(resultSet);
        this.expire = resultSet.getLong("expire");
    }

    @Override
    protected void checkIn(Station station) {
    }

    @Override
    protected void checkOut(float fare) {
    }

    public void checkIn() {
        status = IN_STATION;
        if (isNew()) {
            expire = AFCUtils.getExpireFromCurrent();
        }
    }

    public void checkOut() {
        status = isExpired() ? DESTROYED : OUT_STATION;
    }

    @Override
    public String toInfo() {
        return "\nType: 24h ticket - ID: " + id + " - Valid until : " + AFCUtils.getTime(expire) + "\n";
    }

    @Override
    public String toString() {
        if (isNew() || isDestroyed()) {
            return barCode + ": 24h tickets: " + status;
        }
        return barCode + ": 24h tickets: Valid until " + AFCUtils.getTime(expire);
    }

    @Override
    public boolean isNew() {
        return super.isNew() || expire == 0;
    }

    @Override
    public String errorStatusMessage(String action) {
        StringBuilder builder = new StringBuilder(Messages.Ticket.ERROR_INVALID_24h_TICKET);

        if (isDestroyed()) {
            return builder.append(Messages.Ticket.ERROR_DESTROYED_TICKET).toString();
        }

        switch (action) {
            case CheckingType.CHECK_IN:
                if (isExpired()) {
                    return builder.append(Messages.Ticket.ERROR_EXPIRED_24h_TICKET)
                            .append(AFCUtils.getTime(System.currentTimeMillis()))
                            .toString();
                }
                if (isInStation() || isDestroyed()) {
                    return builder.append(Messages.Ticket.ERROR_NEED_CHECK_OUT).toString();
                }
                break;
            case CheckingType.CHECK_OUT:
                if (!isInStation()) {
                    return builder.append(Messages.Ticket.ERROR_NEED_CHECK_IN).toString();
                }
                break;
        }
        return null;
    }

    /**
     * Check if dayTicket is expire
     *
     * @return boolean
     */
    public boolean isExpired() {
        return expire != 0 && expire < System.currentTimeMillis();
    }

    public String getUpdateQuery(String formatString) {
        return String.format(formatString, status, expire, id);
    }
}
