/**
 * @author Tran Quang Huy
 * @version 1
 * @since 07/12/2019
 */
package edu.hust.afc.entity.fare;

public interface Fare {
    /**
     * get fare
     */
    float getFare();
}
