/**
 * @author Tran Quang Huy
 * @version 1
 * @since 07/12/2019
 */
package edu.hust.afc.entity.travling.certificate;

import com.sun.istack.NotNull;
import edu.hust.afc.entity.station.Station;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class TravelingCertificate {
    /**
     * id of ticket
     */
    protected String id;

    /**
     * barCode of ticket
     */
    protected String barCode;

    public TravelingCertificate(String id, String barCode) {
        this.id = id;
        this.barCode = barCode;
    }

    public TravelingCertificate(@NotNull ResultSet resultSet) throws SQLException {
        this.id = resultSet.getString("id");
        this.barCode = resultSet.getString("bar_code");
    }

    /**
     * Get id of certificate
     *
     * @return String
     */
    public String getId() {
        return id;
    }

    /**
     * Set id of certificate
     *
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Get barcode of certificate
     *
     * @return String
     */
    public String getBarCode() {
        return barCode;
    }

    public boolean isReadyToPay(float payment) {
        return true;
    }

    /**
     * Format info of certificate for displaying
     *
     * @return String
     */
    public abstract String toInfo();

    protected abstract void checkIn(Station station);

    protected abstract void checkOut(float fare);
}
