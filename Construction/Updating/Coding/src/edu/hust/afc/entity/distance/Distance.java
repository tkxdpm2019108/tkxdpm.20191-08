package edu.hust.afc.entity.distance;

public interface Distance {

    /**
     * get distance
     */
    float getDistance();
}
