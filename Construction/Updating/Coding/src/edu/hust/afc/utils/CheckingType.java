/**
 * Class of mutual handle functions
 *
 * @author Loy Kakda
 * @Version 1.0
 * @since 08/11/2019
 */

package edu.hust.afc.utils;

public interface CheckingType {
    String CHECK_IN = "1";
    String CHECK_OUT = "2";
}
