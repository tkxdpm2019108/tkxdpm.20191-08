/**
 * @author Tran Quang Huy
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.observer;

import java.util.ArrayList;
import java.util.List;

public class Subject<T> {
    private List<Observer<T>> observers = new ArrayList<>();

    /**
     * attach a observer to subject
     *
     * @param observer observer attached
     */
    public void attach(Observer<T> observer) {
        observers.add(observer);
    }

    /**
     * detach a observer to subject
     *
     * @param observer observer detach
     */
    public void detach(Observer<T> observer) {
        observers.remove(observer);
    }

    /**
     * notify if data change
     *
     * @param data changed data
     */
    public void notifyChange(T data) {
        for (Observer<T> observer : observers) {
            observer.update(data);
        }
    }
}
