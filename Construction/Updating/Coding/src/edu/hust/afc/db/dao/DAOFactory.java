package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;

public class DAOFactory {

    public static final String STATION = "StationDAO";
    public static final String CODE = "CodeDAO";
    public static final String PREPAID_CARD = "PrepaidCardDAO";
    public static final String ONE_WAY_TICKET = "OnewayTicketDAO";
    public static final String DAY_TICKET = "DayTicketDAO";
    public static final String DEALING_PREPAID_CARD = "DealingPrepaidCardDAO";
    public static final String DEALING_ONE_WAY_TICKET = "DealingOnewayTicketDAO";
    public static final String DEALING_DAY_TICKET = "DealingDayTicketDAO";

    private static DAOFactory instance;

    private DAOFactory() {
    }

    public static DAOFactory getInstance() {
        if (instance == null) {
            instance = new DAOFactory();
        }
        return instance;
    }

    public DAO getDAO(String type) {
        if (type == null) return null;

        switch (type) {
            case STATION:
                return new StationDAOImpl(DBConnection.getInstance());
            case CODE:
                return new CodeDAOImpl(DBConnection.getInstance());
            case PREPAID_CARD:
                return new PrepaidCardDAOImpl(DBConnection.getInstance());
            case ONE_WAY_TICKET:
                return new OnewayTicketDAOImpl(DBConnection.getInstance());
            case DAY_TICKET:
                return new DayTicketDAOImpl(DBConnection.getInstance());
            case DEALING_PREPAID_CARD:
                return new DealingPrepaidCardDAOImpl(DBConnection.getInstance());
            case DEALING_ONE_WAY_TICKET:
                return new DealingOnewayTicketDAOImpl(DBConnection.getInstance());
            case DEALING_DAY_TICKET:
                return new DealingDayTicketDAOImpl(DBConnection.getInstance());
            default:
                return null;
        }
    }
}
