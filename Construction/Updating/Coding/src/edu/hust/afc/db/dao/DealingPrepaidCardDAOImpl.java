/**
 * @author Tran Thi Thu Huong
 * @version 1
 * @since 25/10/2019
 */
package edu.hust.afc.db.dao;

import edu.hust.afc.db.DBConnection;
import edu.hust.afc.entity.dealing.DealingCard;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.sun.istack.NotNull;

public class DealingPrepaidCardDAOImpl implements DealingCardDAO {

    /**
     * Instance of DBConnection help connect with database
     */
    private DBConnection dbConnection;

   	public DealingPrepaidCardDAOImpl(DBConnection dbConnection) {
   		this.dbConnection = dbConnection;
   	}

    @Override
    public boolean save(@NotNull DealingCard dealingCard) {
        String query = "SELECT id FROM dealing_card ORDER BY id DESC LIMIT 1";
        try {
            Connection connection = dbConnection.getConnection();
            ResultSet resultSet = connection.prepareStatement(query).executeQuery();
            int index = resultSet.next() ? resultSet.getInt("id") + 1 : 0;
            connection.prepareStatement(dealingCard.getInsertQuery("dealing_card", index)).executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
