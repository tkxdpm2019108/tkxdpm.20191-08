package testing.java.afc.entity;

import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.ticket.OnewayTicket;
import edu.hust.afc.utils.CheckingType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

public class OnewayTicketTest {
    private Station firstStation;
    private Station secondStation;
    private Station enterStationLessFirst;
    private Station enterStationMiddle;
    private Station enterStationGreaterSecond;
    private OnewayTicket onewayTicketNew;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        firstStation = new Station(4, "Chatelet", 11.3f);
        secondStation = new Station(8, "Bibliotheque Francois Mitterrand", 25.3f);
        enterStationMiddle = new Station(6, "Bercy", 18.9f);
        enterStationLessFirst = new Station(1, "Saint-Lazare", 0f);
        enterStationGreaterSecond = new Station(9, "Olympiades", 28.8f);

        onewayTicketNew = new OnewayTicket("OW201902030000", "New", 3.9f, firstStation, secondStation, "fkdodirn");
    }

    @Test
    public void testGetFirstStation() {
        Assert.assertEquals(firstStation, onewayTicketNew.getFirstStation());
    }

    @Test
    public void testSetFirstStation() {
        Assert.assertNotEquals(secondStation, onewayTicketNew.getFirstStation());
        onewayTicketNew.setFirstStation(secondStation);
        Assert.assertEquals(secondStation, onewayTicketNew.getFirstStation());
    }

    @Test
    public void testGetSecondStation() {
        Assert.assertEquals(secondStation, onewayTicketNew.getSecondStation());
    }

    @Test
    public void testSetSecondStation() {
        Assert.assertNotEquals(firstStation, onewayTicketNew.getSecondStation());
        onewayTicketNew.setSecondStation(firstStation);
        Assert.assertEquals(firstStation, onewayTicketNew.getSecondStation());
    }

    @Test
    public void testGetBalance() {
        Assert.assertEquals(3.9f, onewayTicketNew.getBalance(), 0.1f);
    }

    @Test
    public void testToInfo() {
        String expected = "\nType: One-way ticket\nID: OW201902030000\nBalance: 3.9 euros\n";
        Assert.assertEquals(expected, onewayTicketNew.toInfo());
    }

    @Test
    public void testToString() {
        String expected = "fkdodirn: One-way ticket between Chatelet and Bibliotheque Francois Mitterrand: New - 3.9 euros";
        Assert.assertEquals(expected, onewayTicketNew.toString());
    }

    @Test
    public void errorStatusMessage() {
        Assert.assertNull(onewayTicketNew.errorStatusMessage(CheckingType.CHECK_IN));
        Assert.assertNotNull(onewayTicketNew.errorStatusMessage(CheckingType.CHECK_OUT));
    }

    @Test
    public void testErrorPlatformAreaMessage() {
        Assert.assertNotNull(onewayTicketNew.errorPlatformAreaMessage(enterStationLessFirst));
        Assert.assertNull(onewayTicketNew.errorPlatformAreaMessage(firstStation));
        Assert.assertNull(onewayTicketNew.errorPlatformAreaMessage(enterStationMiddle));
        Assert.assertNull(onewayTicketNew.errorPlatformAreaMessage(secondStation));
        Assert.assertNotNull(onewayTicketNew.errorPlatformAreaMessage(enterStationGreaterSecond));
    }
}