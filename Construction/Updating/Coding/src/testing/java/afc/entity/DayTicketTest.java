package testing.java.afc.entity;

import edu.hust.afc.entity.travling.certificate.ticket.DayTicket;
import edu.hust.afc.utils.CheckingType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DayTicketTest {

    private DayTicket expiredTicket;
    private DayTicket newTicket;
    private DayTicket inUseTicket;

    @Before
    public void setUp() throws Exception {
        inUseTicket = new DayTicket("TF201905060000", "Out station", "kdifornd", 1573319530912L);
        newTicket = new DayTicket("TF201902010001", "New", "oepwlguc", 0);
        expiredTicket = new DayTicket("TF201906130003", "In station", "lkjhgfds", 1573021483271L);
    }

    @Test
    public void toInfo() {
        String expected = "Type: 24h ticket - ID: TF201905060000 - Valid until : 00:12 - 10/11/2019";
        Assert.assertEquals(expected, inUseTicket.toInfo().trim());
    }

    @Test
    public void getExpire() {
        Assert.assertEquals(1573021483271L, expiredTicket.getExpire());
        Assert.assertEquals(0, newTicket.getExpire());
        Assert.assertEquals(1573319530912L, inUseTicket.getExpire());
    }

    @Test
    public void setExpire() {
    }

    @Test
    public void testToString() {
        String expected = "lkjhgfds: 24h tickets: Valid until 13:24 - 06/11/2019";
        String expectedNew = "oepwlguc: 24h tickets: New";
        Assert.assertEquals(expected, expiredTicket.toString());
        Assert.assertEquals(expectedNew, newTicket.toString());
    }

    @Test
    public void isNew() {
        Assert.assertTrue(newTicket.isNew());
        Assert.assertFalse(expiredTicket.isNew());
        Assert.assertFalse(inUseTicket.isNew());
    }

    @Test
    public void errorStatusMessage() {
//        String destroyedMsg = "Invalid 24h Ticket\nYour ticket was destroyed !";
//        String expiredMsg = "Invalid 24h Ticket\nExpired: Try to enter at ";
//        String newMsg = "Invalid 24h Ticket\nTicket is new, You must check in first!";
//        Assert.assertEquals(newMsg, newTicket.errorStatusMessage(CheckingType.CHECK_OUT));
//        Assert.assertTrue(expiredTicket.errorStatusMessage(CheckingType.CHECK_IN).contains(expiredMsg));

    }

    @Test
    public void isExpired() {
        Assert.assertFalse(newTicket.isExpired());
        Assert.assertTrue(inUseTicket.isExpired());
        Assert.assertTrue(expiredTicket.isExpired());
    }
}