package testing.java.afc.entity;

import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.card.PrepaidCard;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

public class PrepaidCardTest {

    private PrepaidCard fakeInStationPrepaidCard;
    private PrepaidCard fakeNewPrepaidCard;
    private Station station;

    @Before
    public void setUp() throws Exception {
    	MockitoAnnotations.initMocks(this);
    	
        station = new Station(3, "Pyramides", 8.5f);

        fakeNewPrepaidCard = new PrepaidCard("MNBVCXZA", "PC201905080000", 18.5f, null, null);
        fakeInStationPrepaidCard = new PrepaidCard("PTOUGMNO", "PC201904120002", 0f, "IN", station);
    }

    @Test
    public void isCardId() {
        Assert.assertTrue(PrepaidCard.isCardId("PC201905080000"));
        Assert.assertFalse(PrepaidCard.isCardId("TW201905080000"));
        Assert.assertFalse(PrepaidCard.isCardId("PC20190508000012"));
    }

    @Test
    public void testToString() {
        String expected = "PTOUGMNO: Prepaid card: 0.0 euros";
        Assert.assertEquals(expected, fakeInStationPrepaidCard.toString());
    }

    @Test
    public void toInfo() {
        String expected = "Prepaid Card - ID: PC201905080000 - Balance: 18.5 euros";
        Assert.assertEquals(expected, fakeNewPrepaidCard.toInfo());
    }

    @Test
    public void isInStation() {
        Assert.assertTrue(fakeInStationPrepaidCard.isInStation());
        Assert.assertFalse(fakeNewPrepaidCard.isInStation());
    }

    @Test
    public void getId() {
        Assert.assertEquals("PC201905080000", fakeNewPrepaidCard.getId());
        Assert.assertNotEquals("PC201905080001", fakeNewPrepaidCard.getId());
    }

    @Test
    public void getBalance() {
        Assert.assertEquals(18.5f, fakeNewPrepaidCard.getBalance(), 0.1f);
        Assert.assertEquals(0f, fakeInStationPrepaidCard.getBalance(), 0.1f);
    }

    @Test
    public void getBarCode() {
        Assert.assertEquals("PTOUGMNO", fakeInStationPrepaidCard.getBarCode());
    }

    @Test
    public void setBalance() {
        Assert.assertNotEquals(5f, fakeNewPrepaidCard.getBalance());
        fakeNewPrepaidCard.setBalance(5f);
        Assert.assertEquals(5f, fakeNewPrepaidCard.getBalance(), 0.1f);
    }

    @Test
    public void getEnterStation() {
        Assert.assertEquals(station, fakeInStationPrepaidCard.getEnterStation());
        Assert.assertNull(fakeNewPrepaidCard.getEnterStation());
    }

    @Test
    public void getStatus() {
        Assert.assertEquals(PrepaidCard.CARD_IN_STATION, fakeInStationPrepaidCard.getStatus());
    }

    @Test
    public void setStatus() {
        Assert.assertNotEquals(PrepaidCard.CARD_IN_STATION, fakeNewPrepaidCard.getStatus());
        fakeNewPrepaidCard.setStatus(PrepaidCard.CARD_IN_STATION);
        Assert.assertEquals(PrepaidCard.CARD_IN_STATION, fakeNewPrepaidCard.getStatus());
    }

    @Test
    public void setEnterStation() {
        Assert.assertNotEquals(station, fakeNewPrepaidCard.getEnterStation());
        fakeNewPrepaidCard.setEnterStation(station);
        Assert.assertEquals(station, fakeNewPrepaidCard.getEnterStation());
    }
}