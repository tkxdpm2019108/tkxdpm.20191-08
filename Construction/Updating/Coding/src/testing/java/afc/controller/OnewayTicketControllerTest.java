package testing.java.afc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;

import edu.hust.afc.controller.OnewayTicketController;
import edu.hust.afc.db.DBConnection;
import edu.hust.afc.db.dao.CertificateDAO;
import edu.hust.afc.db.dao.DealingOnewayTicketDAO;
import edu.hust.afc.db.dao.DealingOnewayTicketDAOImpl;
import edu.hust.afc.db.dao.OnewayTicketDAOImpl;
import edu.hust.afc.entity.station.Station;
import edu.hust.afc.entity.travling.certificate.ticket.OnewayTicket;
import edu.hust.afc.observer.Subject;
import org.powermock.reflect.Whitebox;

@RunWith(MockitoJUnitRunner.class)
public class OnewayTicketControllerTest {

	@InjectMocks
	OnewayTicketController onewayTicketController;

	@Mock
	private CertificateDAO<OnewayTicket> onewayTicketDAO;

	@Mock
	private DealingOnewayTicketDAO dealingOnewayTicketDAO;

	@Mock
	private Subject<String> dataDisplay;

	private List<String> fakeCertificateIds = new ArrayList<>();
	private List<OnewayTicket> fakeOnewayTickets = new ArrayList<>();

	private OnewayTicket fakeOnewayTicketNew;
	private OnewayTicket fakeOnewayTicketInStation;
	private OnewayTicket fakeOnewayTicketDestroyed;

	private Station fakeFirstStation;
	private Station fakeSecondStation;
	private Station fakeStationEnterLessFirst;
	private Station fakeStationEnterBetweenFirstSecond;
	private Station fakeStationEnterGreaterSecond;

	@Before
	public void init() {
		fakeCertificateIds.add("OW201902030000");
		fakeCertificateIds.add("OW201902030001");
		fakeCertificateIds.add("OW201902030003");

		fakeFirstStation = new Station(4, "Chatelet", 11.3f);
		fakeSecondStation = new Station(8, "Bibliotheque Francois Mitterrand", 25.3f);
		fakeStationEnterLessFirst = new Station(1, "Saint-Lazare", 0f);
		fakeStationEnterBetweenFirstSecond = new Station(6, "Bercy", 18.9f);
		fakeStationEnterGreaterSecond = new Station(9, "Olympiades", 28.8f);

		fakeOnewayTicketNew = new OnewayTicket("OW201902030000", "New", 3.9f, fakeFirstStation, fakeSecondStation,
				"fkdodirn");
		fakeOnewayTicketInStation = new OnewayTicket("OW201901020001", "In station", 3.9f, fakeFirstStation,
				fakeSecondStation, "ueydmckr");
		fakeOnewayTicketDestroyed = new OnewayTicket("OW201903140003", "Destroyed", 3.9f, fakeFirstStation,
				fakeSecondStation, "poiuytre");

		fakeOnewayTickets.add(fakeOnewayTicketNew);
		fakeOnewayTickets.add(fakeOnewayTicketInStation);
		fakeOnewayTickets.add(fakeOnewayTicketDestroyed);

	}

	@Test
	public void getCertificatesByIds_returnListOnewayTickets() {
		when(onewayTicketDAO.getDataByIds(fakeCertificateIds)).thenReturn(fakeOnewayTickets);

		List<OnewayTicket> expected = fakeOnewayTickets;
		List<OnewayTicket> actual = onewayTicketController.getCertificatesByIds(fakeCertificateIds);
		Assert.assertNotNull(actual);
		Assert.assertNotEquals(0, actual.size());
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void getCertificatesByIds_returnEmpty() {
		when(onewayTicketDAO.getDataByIds(Collections.emptyList())).thenReturn(Collections.emptyList());

		List<OnewayTicket> actual = onewayTicketController.getCertificatesByIds(Collections.emptyList());
		Assert.assertNotNull(actual);
		Assert.assertEquals(0, actual.size());
	}

	@Test
	public void handleCheckIn() {
		Whitebox.setInternalState(onewayTicketController, "onewayTickets", fakeOnewayTickets);
		
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketNew.getBarCode(), fakeStationEnterLessFirst));
		Assert.assertTrue(onewayTicketController.handleCheckIn(fakeOnewayTicketNew.getBarCode(), fakeFirstStation));
		fakeOnewayTicketNew.setStatus(OnewayTicket.NEW);
		Assert.assertTrue(onewayTicketController.handleCheckIn(fakeOnewayTicketNew.getBarCode(), fakeStationEnterBetweenFirstSecond));
		fakeOnewayTicketNew.setStatus(OnewayTicket.NEW);
		Assert.assertTrue(onewayTicketController.handleCheckIn(fakeOnewayTicketNew.getBarCode(), fakeSecondStation));
		fakeOnewayTicketNew.setStatus(OnewayTicket.NEW);
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketNew.getBarCode(), fakeStationEnterGreaterSecond));

		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketInStation.getBarCode(), fakeStationEnterLessFirst));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketInStation.getBarCode(), fakeFirstStation));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketInStation.getBarCode(), fakeStationEnterBetweenFirstSecond));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketInStation.getBarCode(), fakeSecondStation));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketInStation.getBarCode(), fakeStationEnterGreaterSecond));

		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketDestroyed.getBarCode(), fakeStationEnterLessFirst));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketDestroyed.getBarCode(), fakeFirstStation));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketDestroyed.getBarCode(), fakeStationEnterBetweenFirstSecond));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketDestroyed.getBarCode(), fakeSecondStation));
		Assert.assertFalse(onewayTicketController.handleCheckIn(fakeOnewayTicketDestroyed.getBarCode(), fakeStationEnterGreaterSecond));
	}

	@Test
	public void handleCheckOut() {
		when(dealingOnewayTicketDAO.getStationByOnewayTicketId(fakeOnewayTicketInStation.getId())).thenReturn(fakeFirstStation);
		Whitebox.setInternalState(onewayTicketController, "onewayTickets", fakeOnewayTickets);
		
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketNew.getBarCode(), fakeStationEnterLessFirst));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketNew.getBarCode(), fakeFirstStation));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketNew.getBarCode(), fakeStationEnterBetweenFirstSecond));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketNew.getBarCode(), fakeSecondStation));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketNew.getBarCode(), fakeStationEnterGreaterSecond));

		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketDestroyed.getBarCode(), fakeStationEnterLessFirst));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketDestroyed.getBarCode(), fakeFirstStation));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketDestroyed.getBarCode(), fakeStationEnterBetweenFirstSecond));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketDestroyed.getBarCode(), fakeSecondStation));
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketDestroyed.getBarCode(), fakeStationEnterGreaterSecond));

		Assert.assertTrue(onewayTicketController.handleCheckOut(fakeOnewayTicketInStation.getBarCode(), fakeStationEnterLessFirst));
		fakeOnewayTicketInStation.setStatus(OnewayTicket.IN_STATION);
		Assert.assertTrue(onewayTicketController.handleCheckOut(fakeOnewayTicketInStation.getBarCode(), fakeFirstStation));
		fakeOnewayTicketInStation.setStatus(OnewayTicket.IN_STATION);
		Assert.assertTrue(onewayTicketController.handleCheckOut(fakeOnewayTicketInStation.getBarCode(), fakeStationEnterBetweenFirstSecond));
		fakeOnewayTicketInStation.setStatus(OnewayTicket.IN_STATION);
		Assert.assertTrue(onewayTicketController.handleCheckOut(fakeOnewayTicketInStation.getBarCode(), fakeSecondStation));
		fakeOnewayTicketInStation.setStatus(OnewayTicket.IN_STATION);
		Assert.assertFalse(onewayTicketController.handleCheckOut(fakeOnewayTicketInStation.getBarCode(), fakeStationEnterGreaterSecond));
	}
}
